### rpm2pkg

Simple Python application for converting of *.rpm* to *arch's .pkg.tar.gz*

### Usage
1. Install python and system requirements
```bash
sudo pacman -S rpm rpm2cpio cpio git
python3 -m pip install --upgrade --user pip pyyaml jinja2
```
2. Clone repository
```bash
git clone --depth 1 --single-branch git@gitlab.com:vlasov-y/rpm2pkg.git 
cd rpm2pkg
```
3. Convert rpm to pkg
```bash
curl -fLO https://repo.skype.com/latest/skypeforlinux-64.rpm
python3 rpm2pkg skypeforlinux-64.rpm
# or add --install to install package automatically (interactive mode, will prompt you for confirmation)
# python3 rpm2pkg --install skypeforlinux-64.rpm
```
4. Install
```bash
sudo pacman -U skype*.pkg.tar.gz
```
5. Remove
```bash
sudo pacman -R skype
```
